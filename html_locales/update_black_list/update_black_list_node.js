// SPDX-Filename: update_black_list.js
// SPDX-FileCopyrightText: 2019 Simon Descarpentres <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only

/* globals require */

'use strict'
const fs = require('fs')
const bl_json = JSON.parse(fs.readFileSync('html_locales/black_list_manual_part.json', 'utf8'))
const src_json = JSON.parse(fs.readFileSync('json/sources.json', 'utf8'))
const src_list = Object.values(src_json)
// const intl_lang_name = (lg) => new Intl.DisplayNames([lg], {type: 'language'}).of(lg)
const eng_lang_name    = (lg) => new Intl.DisplayNames(['en'], {type: 'language'}).of(lg)
const eng_country_name = (lg) => new Intl.DisplayNames(['en'], {type: 'region'}).of(lg)

for (let src of src_list) {
	try {
		bl_json[src.tags.name] = ''
		if (typeof(src.tags.lang) !== 'undefined') {  // sources of extends type can miss some tags
			bl_json[`[${src.tags.lang}]`] = ''
			bl_json[eng_lang_name(src.tags.lang)] = ''
		}
		if (typeof(src.tags.country) !== 'undefined') {
			bl_json[`[${src.tags.country}]`] = ''
			bl_json[eng_country_name(src.tags.country)] = ''
			console.log('country', eng_country_name(src.tags.country)) // node seem to be missing this
		}
	} catch (exc) {
		console.log(exc)
		console.log(src.tags)
	}
}
for (let i = src_list.length; i--;) {
	bl_json[`${i}`] = ''
	bl_json[`(${i})`] = ''  // can't be more source of a categorie than all the sources at once
}
fs.writeFileSync('html_locales/black_list.json', JSON.stringify(bl_json, null, '\t'))
